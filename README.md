﻿# <center> Prácticas Metaheuristicas 2019/20

Universidad de Jaén</br>
Grupo 8:30</br>

*******
## Descripción
El Proyecto está compuesto por varios ficheros de C#, entre ellos el principal y los diferentes objetos que hemos necesito a largo del proceso:

* **Airport.cs**: Representa el aeropuerto y se encarga de cargar las matrices desde los ficheros de datos con el formato dado para el problema.
* **Cola.cs**: Es una EEDD utilizada para la memoria a corto plazo para la busqueda Tabú, es una cola estándar de C# con tamaño limitado.
* **Config.cs**: Es el archivo encargado de leer y cargar los parametros para los diferentes algoritmos.
* **Logger.cs**: En este fichero se encuentra el objeto encargado de escribir datos a los ficheros de log.
* **Utility.cs**: Clase auxiliar que inclueye funciones plantilla y funciones comunes a todos los algoritmos o utilidades.
* **Pr1MH.cs**: Clase principal, se encaga de lanzar los hilos y cargar las configuraciones.
* El resto de ficheros son los diferentes algoritmos con sus funciones auxiliares internas y EEDD necesarias.

El funcionamiento del programa es simple, en el fichero de parámetros se cargan los diferentes datos para el programa, una vez empiece, si todo se carga correctamente no aparecerá nada hasta el siguiente promt del sistema.</br>
La sintaxis del archivo de parametros está detallada dentro de este con comentarios. </br>
>**NOTA**: El programa lanzará tantos hilos como la mitad de hilos de procesamiento que tenga el sistema, para sistemas de 16 hilos, lanzará ficheros de 8 en 8 hasta terminar todos.

Todas las pruebas incluidas en el informe y tablas se han realizado en un Ryzen 7 2700 8 Núcleos/16 Hilos en MacOS Catalina 10.15.1, es probable que la velocidad de las ejecuciones se vea afectada por las diferencias en cuenta de núcleos, IPC del procesador y Sistema operativo.

Una vez ejecutado el programa, la carpeta Logs deberia aparecer junto al .exe, en esta carpeta se encuentran todos los logs, que tendrán el siguiente formato: </br>
**algoritmo_semilla_fichero**</br>
(El fichero es el número relativo a los pasados por parametros)
> Los algoritmos genéticos reciben parametros, los aceptados son el número de individuos élite y el cruce MOC o OX2.

*********
## Cómo usarlo:
* **Windows**: Lanzarlo desde la terminal con el fichero de parametros como argumento.
* **MacOS X**: Utilizar el script .sh incluido (Es posible que falle, en caso de fallar introducir el comando como en Linux).
* **GNU/Linux**: Lanzarlo desde la terminal utilizando el siguiente comando: mono Pr1MH.exe param.txt para cargar el fichero de párametros por defecto.
> **NOTA**: Ya que el programa está escrito en C# y el resultado de la compilación es un .exe, para ejecutarlo en Linux/Mac es necesario tener instalado el Mono framework, se puede obtener desde el siguiente enlace: [Mono Framework](https://www.mono-project.com/download/stable/ "Stable release").
> </br>Visual Studio para MacOS incluye Mono Framework.