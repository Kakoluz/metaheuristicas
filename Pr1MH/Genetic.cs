﻿using System;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.Generic;

namespace Pr1MH
{
    internal static class Constants { 
        public const int MAX_POB = 50;
        public const float CRUC_PROB = 0.7f;
        public const float MUT_PROB = 0.05f;
    }
    internal class Individual
    {
        private int[] solution;
        private int cost;
        private int gen;
        private bool elite;
        private bool evaluated;
        private Airport _airport;
        public int Length;

        public Individual(Airport air, int[] sol, int generation)
        {
            solution = new int[sol.Length];
            Array.Copy(sol, solution, sol.Length);
            _airport = air;
            gen = generation;
            elite = false;
            Length = solution.Length;
            evaluated = false;
        }

        public int getCost()
        {
            return cost;
        }

        public int getGen()
        {
            return gen;
        }

        public bool isElite()
        {
            return elite;
        }

        public int this[int key]
        {
            get => solution[key];
        }

        public void turnElite()
        {
            elite = true;
        }

        public void resetElite()
        {
            elite = false;
        }

        public bool evaluar()
        {
            bool returnValue = !evaluated;
            if (!evaluated)
            {
                cost = Utility.evaluacion(_airport, solution);
                evaluated = true;
            }
            return returnValue;
        }

        public int[] toArray()
        {
            int[] newAray = new int[solution.Length];
            Array.Copy(solution, newAray, solution.Length);
            return newAray;
        }

        public bool Equals(Individual other)
        {
            if (this.cost == other.cost)
                return true;
            else
                return false;
        }

        public static bool operator <(Individual left, Individual right)
        {
            if (left.cost < right.cost)
                return true;
            else
                return false;
        }

        public static bool operator >(Individual left, Individual right)
        {
            if (left.cost > right.cost)
                return true;
            else
                return false;
        }

        public static bool operator == (Individual left, Individual right)
        {
            if (left.solution == right.solution)
                return true;
            else
                return false;
        }

        public static bool operator !=(Individual left, Individual right)
        {
            return !(left == right);
        }

    }

    static class Genetic
    {
        public static void GeneticAlg (Airport _airport, Config _config, int fileIndex, System.Threading.CountdownEvent countdown)
        {
            string ruta = Utility.file_path(_config, fileIndex); //Genera la ruta del fichero según el Sistema Operativo
            Logger file = new Logger(@ruta);//crea el archivo de escritura

            /* Escribimos la información del fichero */
            file.WriteLine("Se va a ejecutar el fichero: " + _config.getFile(fileIndex)); //se escriben los datos del archivo de datos
            file.WriteLine("Semilla: " + _config.getSeed());
            file.WriteLine("Elite: " + _config.getElite());
            file.WriteLine("Cruce: " + _config.getCru());
            file.WriteLine(DateTime.Now.ToString());
            file.WriteLine("");
            file.WriteLine("");

            //Inicia el cronometro
            Stopwatch sw = new Stopwatch();
            sw.Start();

            
            Individual[] individuals = new Individual[Constants.MAX_POB];
            int size = _airport.getSize();
            Random n = new Random((int)_config.getSeed());
            int iterations = Constants.MAX_POB;
            int numCruces;
            int numMutaciones;
            int generation = 1;
            int[] repetidos = new int[size];
            for (int i = 0; i < Constants.MAX_POB; i++) //inicialización de los miembros
            {
                int[] mejor = new int[size];
                for (int j = 0; j < size; ++j)
                {
                    mejor[j] = j;
                }
                int x = n.Next(size);
                for (int j = 0; j < x; ++j)
                {
                    int aux1 = n.Next(size);
                    int aux2 = n.Next(size);
                    while (aux1 == aux2)
                        aux2 = n.Next(size);
                    Utility.swap(ref mejor[aux1], ref mejor[aux2]);
                }
                individuals[i] = new Individual(_airport, mejor, 1);
            }
            Individual bestGen = individuals[0], Best = individuals[0];
            for (int i = 0; i < Constants.MAX_POB; i++)
            {
                individuals[i].evaluar();
            }
            while (iterations < _config.getIterations()) //Inicio del bucle genetico
            {
                generation++;
                numCruces = 0;
                numMutaciones = 0;
                Individual[] midGeneration = new Individual[individuals.Length];
                Array.Copy(tournament2(n, ref individuals), midGeneration, midGeneration.Length); //Generamos la poblacion intermedia por toneo binario
                for (int i = 0; i < Constants.MAX_POB; i+=2) //Recorremos todos los individios para buscar cruces
                {
                    if (n.NextDouble() <= Constants.CRUC_PROB)
                    {
                        if (_config.getCru() == "moc")
                        {
                            int[] otherSon;
                            midGeneration[i] = new Individual(_airport, MOC(n, midGeneration[i], midGeneration[i+1], out otherSon), generation);
                            midGeneration[i+1] = new Individual(_airport, otherSon, generation);
                        }
                        else if (_config.getCru() == "ox2")
                        {
                            Individual aux = midGeneration[i];
                            midGeneration[i] = new Individual(_airport, OX2(n, midGeneration[i], midGeneration[i+1]), generation);
                            midGeneration[i] = new Individual(_airport, OX2(n, midGeneration[i+1], aux), generation);
                        }
                        numCruces++;
                    }
                }
                for (int i = 0; i < Constants.MAX_POB; i++) //Recorremos los individuos cruzados para buscar mutaciones
                {
                    if (n.NextDouble() <= Constants.MUT_PROB)
                    {
                        int[] copy = midGeneration[i].toArray();
                        mutate(n, ref copy);
                        for (int j = 0; j < copy.Length; j++)
                        {
                            for (int h = 0; h < copy.Length; h++)
                            {
                                if (copy[j] == copy[h])
                                    Console.WriteLine();
                            }
                        }
                        midGeneration[i] = new Individual(_airport, copy, generation);
                        numMutaciones++;
                    }
                }
                iterations += evaluateAll(midGeneration); //Evaluamos los individuos que no se hayan evaluado antes
                for (int i = 0; i < _config.getElite(); i++) //Buscamos la élite en la población inicial y el peor en la población intermedia
                {
                    int best = 0;
                    int worst = 0;
                    for (int j = 0; j < Constants.MAX_POB; j++) //Buscamos el mejor y el peor en ambas estructuras a la vez, omitimos los mejores ya cogidos
                    {
                        if (individuals[j] < individuals[best] && !individuals[j].isElite())
                            best = j;
                        if (midGeneration[j] > midGeneration[worst])
                            worst = j;
                    }
                    individuals[best].turnElite();
                    for (int j = 0; j < Constants.MAX_POB; j++) //Sustituimos el peor con el elite encontrado, o sustituimos el elite de la poblacion intermedia por el que tiene los valores ya asignados de la población anterior
                    {
                        if (individuals[best] == midGeneration[i])
                        {
                            midGeneration[i].turnElite();
                        } else if (j == Constants.MAX_POB)
                        {
                            midGeneration[worst] = individuals[best];
                        }
                    }
                }
                Array.Copy(midGeneration, individuals, individuals.Length); //Sustituimos la generación por la intermedia
                for (int i = 0; i < Constants.MAX_POB; i++)
                {
                    if (bestGen > individuals[i])
                        bestGen = individuals[i];
                }
                if (bestGen < Best)
                    Best = bestGen;
                file.WriteLine("---------------------------------------------------------------");
                file.WriteLine("Coste mejor: " + bestGen.getCost());
                file.WriteLine("Generacion: " + generation);
                file.WriteLine("Cruces: " + numCruces);
                file.WriteLine("Mutaciones: " + numMutaciones);
                resetAll(individuals); //Reiniciamos todos los élites
            }
            sw.Stop();
            string bestStr = string.Join(" ", Best.toArray());
            file.WriteLine("---------------------------------------------------------------");
            file.WriteLine("Fichero: " + _config.getFile(fileIndex));
            file.WriteLine("La solución obtenida es: " + bestStr);
            file.WriteLine("Costo: " + Best.getCost());
            file.WriteLine("Evaluaciones: " + iterations);
            file.WriteLine("Generación: " + Best.getGen());
            file.WriteLine("Ha tardado: " + sw.ElapsedMilliseconds + "ms");
            file.Flush();
            countdown.Signal();
        }//fin del algoritmo

        private static int[] OX2 (Random _n, Individual padreA, Individual padreB) //Devuelve la solución del hijo
        {
            List<int> list = new List<int>();
            for (int i = 0; i < padreA.Length; i++)
            {
                if (_n.NextDouble() > 0.5f)
                {
                    list.Add(padreA[i]);
                }
            }
            int[] returnValue = new int[padreB.Length];
            int listIte = 0;
            for (int i = 0; i < padreB.Length; i++)
            {
                if (list.Contains(padreB[i]))
                {
                    returnValue[i] = list[listIte];
                    listIte++;
                }
                else
                    returnValue[i] = padreB[i];
            }
            return returnValue;
        }

        private static int[] MOC (Random _n, Individual padreA, Individual padreB, out int[] secondSon)
        {
            int point = _n.Next(1, padreA.Length-1);
            int[] firstSon = new int[padreA.Length];
            secondSon = new int[padreA.Length];
            List<int> Avalues = new List<int>();
            List<int> Bvalues = new List<int>();
            for (int i = point; i < padreB.Length; i++)
            {
                Avalues.Add(padreA[i]);
                Bvalues.Add(padreB[i]);
            }
            int aIte = 0, bIte = 0;
            for (int i = 0; i < padreA.Length; i++)
            {
                if (Avalues.Contains(padreB[i]))
                {
                    firstSon[i] = Avalues[aIte];
                    aIte++;
                }
                else
                    firstSon[i] = padreB[i];

                if (Bvalues.Contains(padreA[i]))
                {
                    secondSon[i] = Bvalues[bIte];
                    bIte++;
                }
                else
                    secondSon[i] = padreA[i];
            }
            return firstSon;
        }

        private static void mutate(Random _n, ref int[] array)
        {
            int[] positions = new int[3];
            for (int i = 0; i < array.Length; i++)
            {
                if (_n.NextDouble() < Constants.MUT_PROB)
                {
                    positions[0] = i;
                    positions[1] = _n.Next(array.Length);
                    positions[2] = _n.Next(array.Length);
                    while (positions[1] == positions[0])
                        positions[1] = _n.Next(array.Length);
                    while (positions[1] == positions[2] || positions[2] == positions[0])
                        positions[2] = _n.Next(array.Length);

                    Utility.tresOpt(ref array, positions);
                }
            }
        }

        private static Individual[] tournament2 (Random _n, ref Individual[] members)
        {
            Individual[] retorno = new Individual[members.Length];
            for (int i = 0; i < members.Length; i++)
            {
                int aux1 = _n.Next(members.Length);
                int aux2 = aux1;
                while (aux2 == aux1)
                    aux2 = _n.Next(members.Length);
                if (members[aux1] < members[aux2])
                    retorno[i] = members[aux1];
                else
                    retorno[i] = members[aux2];
            }
            return retorno;
        }

        private static int evaluateAll (Individual[] individuals)
        {
            int evaluations = 0;
            for (int i = 0; i < Constants.MAX_POB; i++)
            {
                if (individuals[i].evaluar())
                    evaluations++;
            }
            return evaluations;
        }

        private static void resetAll (Individual[] individuals)
        {
            for (int i = 0; i < Constants.MAX_POB; i++)
            {
                individuals[i].resetElite();
            }
        }
    }
}
