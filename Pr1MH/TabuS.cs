﻿using System;
using System.Diagnostics;

namespace Pr1MH
{
    static class TabuS
    {
        private static int[] intersificar(int[,] matriz, int size) //Escoge la solución más explorada
        {
            int[] sol = new int[size];
            for (int i = 0; i < sol.Length; i++)
                sol[i] = -1;

            for (int puerta = 0; puerta < size; puerta++)
            {
                int mayor = Int32.MinValue;
                int pos=-1;
                for (int posicion = 0; posicion < size; posicion++)
                {
                    if(matriz[puerta,posicion] > mayor && sol[posicion] == -1)
                    {
                        mayor = matriz[puerta, posicion];
                        pos = posicion;
                    }
                }
                sol[pos] = puerta;
            }
            return sol;
        }

        private static int[] diversificar(int[,] matriz, int size) //Escoge la solución menos explorada
        {
            int[] sol = new int[size];
            for (int i = 0; i < sol.Length; i++)
                sol[i] = -1;

            for (int puerta = 0; puerta < size; puerta++)
            {
                int menor = Int32.MaxValue;
                int pos = -1;
                for (int posicion = 0; posicion < size; posicion++)
                {
                    if (matriz[puerta, posicion] < menor && sol[posicion] == -1)
                    {
                        menor = matriz[puerta, posicion];
                        pos = posicion;
                    }
                }
                sol[pos] = puerta;
            }
            return sol;
        }

        public static void TabuSearch(Airport _airport, Config _config, int fileIndex, System.Threading.CountdownEvent countdown)
        {
            try
            {
                Cola<int[]> ShortTermMemory = new Cola<int[]>(5);//memoria a corto plazo con las soluciones tabú
                int[,] LargoPlazo = new int[_airport.getSize(), _airport.getSize()]; // Memoria a largo plazo           
                string ruta = Utility.file_path(_config, fileIndex); //Genera la ruta del fichero según el Sistema Operativo
                Logger file = new Logger(@ruta);//crea el archivo de escritura
                int intesificado = 0; //Contador de usos de la memoria a largo plazo y modo
                int diversificado = 0;

                /* Escribimos la información del fichero */
                file.WriteLine("Se va a ejecutar el fichero: " + _config.getFile(fileIndex)); //se escriben los datos del archivo de datos
                file.WriteLine("semilla: " + _config.getSeed());
                file.WriteLine(DateTime.Now.ToString());
                file.WriteLine("");
                file.WriteLine("");

                //Inicia el cronometro
                Stopwatch sw = new Stopwatch();
                sw.Start();

                //se genera la primera la solución de forma aleatoria 
                int[] mejor = new int[_airport.getSize()];
                for (int i = 0; i < mejor.Length; ++i)
                    mejor[i] = i;

                Random n = new Random((int)_config.getSeed());
                int x = (n.Next() % (_airport.getSize()));

                for (int i = 0; i < x; ++i)
                {
                    Utility.swap(ref mejor[n.Next(mejor.Length)], ref mejor[n.Next(mejor.Length)]);
                }
                int costomejor = Utility.evaluacion(_airport, mejor);

                //se muestra la primera solución generada al azar
                string str = string.Join(" ", mejor);
                file.WriteLine("Solución inicial: " + str);
                file.WriteLine("Costo: " + costomejor);
                file.WriteLine("It: 0");

                //generar la estructura para los vencinos
                int nvecinos = (_airport.getSize() * (_airport.getSize() - 1)) / 2;
                Tuple<int, int>[] vecino = new Tuple<int, int>[nvecinos];
                int tamv = 0;
                for (int i = 0; i < _airport.getSize(); i++)
                {
                    for (int j = i + 1; j < _airport.getSize(); j++)
                    {
                        vecino[tamv] = new Tuple<int, int>(i, j);
                        ++tamv;
                    }
                }
                
                int intentos = 0; //Nuemro de movimientos a peor realizados
                int entornos = 0; //Numero de entornos mirados
                int h = 0; //declarado aqui por eficiencia 
                int[] movimiento = new int[_airport.getSize()]; //Mejor de todos los entornos
                int costeMovimiento = costomejor; //Inicializamos el coste del movimiento al mejor
                int it = 0; //Contador de iteraciones
                long iterations = _config.getIterations(); //Para ahorrar accesos al objeto 
                do
                {
                    tamv = nvecinos - 1;
                    int costeMejorVecino = costomejor;
                    int[] mejorvecino = new int[_airport.getSize()]; //Mejor del entorno
                    for (int j = 0; j < 10; j++)
                    {
                        int[] solpos; //Vecino actual a comprara
                        int r, s; //Valores a intercambiar
                        do //Genera vecinos hasta que encuentre uno que no sea tabú
                        {
                            h = n.Next(tamv);
                            r = vecino[h].Item1;
                            s = vecino[h].Item2;

                            solpos = new int[mejor.Length];
                            Array.Copy(mejor, solpos, mejor.Length);

                            Utility.swap(ref solpos[r], ref solpos[s]);
                        } while ((ShortTermMemory.contains(solpos)));

                        ShortTermMemory.push(solpos);
                        Utility.swap(ref vecino[h], ref vecino[tamv]);//lleva el intercambio usado al final y reduce el tamaño para que no salga de nuevo
                        --tamv;

                        if (Utility.evaluacion(_airport, solpos) < costomejor)
                        {
                            costeMejorVecino = Utility.evaluacion(_airport, solpos);
                            Array.Copy(solpos, mejorvecino, mejorvecino.Length);
                            for (int i = 0; i < solpos.Length; i++)//anota la solución en la memoria a largo plazo
                            {
                                LargoPlazo[mejorvecino[i], i]++;
                            }
                        }
                    }
                    if (entornos == 100)
                    {
                        Array.Copy(mejorvecino, movimiento, movimiento.Length);
                        costeMovimiento = costeMejorVecino;

                        file.WriteLine("---------------------------------------------------------------");
                        file.WriteLine("Movimiento: " + vecino[h].Item1 + "," + vecino[h].Item2);
                        file.WriteLine("Costo: " + costeMovimiento);
                        file.WriteLine("It: " + it);
                        it++;
                        intentos++;
                        entornos = 0;
                    }

                    if (costeMovimiento < costomejor)
                    {
                        file.WriteLine("    ***********");
                        file.WriteLine("    Se ha mejorado de " + costomejor + " a " + costeMovimiento);
                        file.WriteLine("    ***********");
                        entornos = 0;
                        Array.Copy(movimiento, mejor, movimiento.Length);
                        costomejor = costeMovimiento;
                        ++it;
                    }
                    else
                        entornos++;

                    if (intentos >= 100)
                    {
                        if (n.NextDouble() < 0.5)
                        {
                            Array.Copy(intersificar(LargoPlazo, _airport.getSize()), movimiento, _airport.getSize() - 1);
                            intesificado++;
                        }
                        else
                        {
                            Array.Copy(diversificar(LargoPlazo, _airport.getSize() - 1), movimiento, _airport.getSize() - 1);
                            diversificado++;
                        }
                        costeMovimiento = Utility.evaluacion(_airport, movimiento);
                        intentos = 0;
                        ShortTermMemory.clear();
                        ShortTermMemory.push(movimiento);
                        it++;
                    }

                } while (it < iterations);
                //Detiene el cronometro, escribe la solución final y cierra el fichero
                sw.Stop();


                string bestStr = string.Join(" ", mejor);
                file.WriteLine("---------------------------------------------------------------");
                file.WriteLine("La solución obtenida es: " + bestStr);
                file.WriteLine("Costo: " + costomejor);
                file.WriteLine("It: " + it);
                file.WriteLine("Diversificaciones: " + diversificado);
                file.WriteLine("Intensificiaciones: " + intesificado);
                file.WriteLine("Ha tardado: " + sw.ElapsedMilliseconds + "ms");
                file.Flush();
                countdown.Signal();
            }
            catch (ArgumentException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
                Console.ResetColor();
            }
        }
    }
}
