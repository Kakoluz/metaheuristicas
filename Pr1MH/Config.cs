﻿using System;
using System.Runtime.InteropServices;
using System.IO;
using System.Collections.Generic;

namespace Pr1MH
{
    class Config
    {
        private bool windows;       //This is used to detect Windows or Unix styled paths
        private long seed;          //seed for the random generator
        private string algorithm;   //The algorith to apply to the files
        private long iterations;    //Number of iterations to do on the specified algorithm defaults to for greedy
        private string[] datafiles; //Path to data files loaded
        private string cruce;       //Operator for crossing in genetic algorithms
        private int elite;          //Number of Elite members in the genetic algorithm

        public Config (string path)
        {
            windows = true;
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                    windows = false;
            try
            {
                string[] lines = System.IO.File.ReadAllLines(@path);
                int fileIndex = 0;
                while (lines[fileIndex].StartsWith("#") || lines[fileIndex] == "")
                {
                    fileIndex++;
                }
                bool error = false;
                for (int i = fileIndex; i < lines.Length && lines[i]==""; i++)
                {
                    if (lines[i][0] == ' ')
                    {
                        error = true;
                    }
                }
                if (error)
                {
                    throw new ArgumentException("Uno o más parametros tienen espacios al principio de la línea");
                }
                Int64.TryParse(lines[fileIndex], out seed);
                fileIndex++;
                algorithm = lines[fileIndex].Split(' ')[0].ToLower();
                if (algorithm == "genetic")
                {
                    try
                    {
                        cruce = lines[fileIndex].Split(' ')[2].ToLower();
                        if (cruce != "moc" && cruce != "ox2")
                            throw new ArgumentException("La función de cruce es erronea");
                        Int32.TryParse(lines[fileIndex].Split(' ')[1], out elite);
                    }
                    catch(IndexOutOfRangeException e)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error: no hay parámetros para el argumento genetico\n");
                        Console.ResetColor();
                        throw new IndexOutOfRangeException(e.Message);
                    }
                }
                fileIndex++;
                if (algorithm == "greedy" || algorithm == "g")
                    iterations = 1;
                else
                    Int64.TryParse(lines[fileIndex], out iterations);
                fileIndex++;
                datafiles = new string[lines.Length - fileIndex];
                Array.Copy(lines, fileIndex, datafiles, 0, lines.Length - fileIndex);

            }
            catch (FileNotFoundException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: el fichero no existe\n" + e.Message);
                Console.ResetColor();
                throw new FileNotFoundException(e.Message);
            }
            catch (IOException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: el fichero está dañado o no se puede leer\n" + e.Message);
                Console.ResetColor();
                throw new IOException(e.Message);
            }
        }

        public bool isWindows()
        {
            return windows;
        }

        public long getSeed()
        {
            return seed;
        }

        public string getAlgorithm()
        {
            return algorithm;
        }

        public long getIterations()
        {
            return iterations;
        }

        public int getFileCount()
        {
            return datafiles.Length;
        }

        public string getFile(int index)
        {
            return datafiles[index];
        }

        public string getCru()
        {
            return cruce;
        }

        public int getElite()
        {
            return elite;
        }
    }
}
