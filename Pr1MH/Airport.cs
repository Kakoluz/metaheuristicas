﻿using System;
using System.Collections.Generic;

namespace Pr1MH
{
    class Airport
    {
        private int[,] flow;
        private int[,] position;
        private int size;
        private bool simetrical;

        public Airport(string path)
        {
            string[] file = System.IO.File.ReadAllLines(path);
            simetrical = true;
            int index = 0;
            List<String> words = new List<string>();
            for (int i = 0; i < file.Length; i++)
            {
                words.AddRange(file[i].Split(' '));
            }
            while (words[index] == "")
            {
                index++;
            }
            Int32.TryParse(words[index], out size);
            index++;
            flow = new int[size, size];
            position = new int[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (words[index] != "")
                    {
                        Int32.TryParse(words[index], out flow[i, j]);
                    }
                    else
                    {
                        j--;
                    }
                    index++;
                }
            }
            index++;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (words[index] != "")
                    {
                        Int32.TryParse(words[index], out position[i, j]);
                    }
                    else
                    {
                        j--;
                    }
                    index++;
                }
            }
            int x = 0;
            int y = 0;
            while (simetrical && x > size)
            {
                while (simetrical && y > size)
                {
                    if (position[x,y] != position[x,y] || flow[x,y] != flow [x,y])
                    {
                        simetrical = false;
                    }
                    y++;
                }
                y = x;
                x++;
            }
        }

        public void showAirport()
        {
            Console.WriteLine("Size: " + size);
            Console.WriteLine("========================================================\nFlujo:\n");
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write(flow[i, j]);
                    Console.Write(" ");
                }
                Console.WriteLine(string.Empty);
            }
            Console.WriteLine(string.Empty);
            Console.WriteLine("========================================================\nPosicion:\n");
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write(position[i, j]);
                    Console.Write(" ");
                }
                Console.WriteLine(string.Empty);
            }
            Console.WriteLine(string.Empty);
            Console.WriteLine("========================================================");
        }

        public int[,] getFlow()
        {
            return this.flow;
        }

        public int[,] getPosition()
        {
            return this.position;
        }

        public int getSize()
        {
            return this.size;
        }

        public int getFlowPos(int x, int y)
        {
            return flow[x, y];
        }

        public int getPosPos(int x, int y)
        {
            return position[x, y];
        }

        public bool isSimetrical()
        {
            return simetrical;
        }
    }
}