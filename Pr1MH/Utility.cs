﻿using System;

namespace Pr1MH
{
    static class Utility
    {
        public static void visualizar(int[] array)
        {
            Console.WriteLine("");
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i] + 1);
                Console.Write(" ");
            }
            Console.WriteLine("");
        }

        public static int evaluacion(Airport _airport, int[] solucion)
        {
            int sum = 0;
            if (_airport.isSimetrical())
            {
                for (int i = 0; i < solucion.Length; i++)
                {
                    for (int j = i; j < solucion.Length; j++)
                    {
                        sum += (_airport.getFlowPos(i, j) * _airport.getPosPos(solucion[i], solucion[j]) * 2);
                    }
                }
            }
            else
            {
                for (int i = 0; i < solucion.Length; i++)
                {
                    for (int j = 0; j < solucion.Length; j++)
                    {
                        sum += _airport.getFlowPos(i, j) * _airport.getPosPos(solucion[i], solucion[j]);
                    }
                }
            }
            return sum;
        }

        public static int factorizacion(Airport _airport, int[] sol, int costo, int i, int j)
        {
            int cambio = 0;
            for (int k = 0; k < _airport.getSize(); k++)
            {
                    cambio += _airport.getFlowPos(i, k) * (_airport.getPosPos(sol[j], sol[k]) - _airport.getPosPos(sol[i], sol[k])) +
                    _airport.getFlowPos(j, k) * (_airport.getPosPos(sol[i], sol[k]) - _airport.getPosPos(sol[j], sol[k])) +
                    _airport.getFlowPos(k, i) * (_airport.getPosPos(sol[k], sol[j]) - _airport.getPosPos(sol[k], sol[i])) +
                    _airport.getFlowPos(k, j) * (_airport.getPosPos(sol[k], sol[i]) - _airport.getPosPos(sol[k], sol[j]));
            }
            return costo - cambio;
        }

        public static void swap<T>(ref T a, ref T b)
        {
            T aux = b;
            b = a;
            a = aux;
        }

        public static string file_path(Config _config, int fileIndex)
        {
            string ruta;
            string formato = _config.getAlgorithm() + "_" + _config.getSeed() + "_" + (fileIndex + 1) + ".txt"; //formato que tendra el nombre del fichero
            if (_config.getAlgorithm() == "genetic")
                formato = _config.getAlgorithm() + "_" + _config.getElite() + "_" + _config.getCru() + "_" + _config.getSeed() + "_" + (fileIndex + 1) + ".txt";
            if (_config.isWindows())
            {
                System.IO.Directory.CreateDirectory(@".\logs\");
                ruta = @".\logs\" + formato;
            }
            else
            {
                System.IO.Directory.CreateDirectory(@"./logs/");
                ruta = @"./logs/" + formato;
            }

            return ruta;
        }

        public static void tresOpt (ref int[] array, int[] positions)
        {
            int aux = array[positions[0]];
            Utility.swap<int>(ref aux, ref array[positions[1]]);
            Utility.swap<int>(ref aux, ref array[positions[2]]);
            Utility.swap<int>(ref aux, ref array[positions[0]]);
        }
    }
}