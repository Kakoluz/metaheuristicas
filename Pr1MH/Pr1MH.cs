﻿using System;
using System.Threading.Tasks;
using System.IO;
using System.Management;

namespace Pr1MH
{
    class Pr1MH
    {
        static void Main(string[] args)
        {
            Config _config = new Config(args[0]); //Cargamos el fichero de configuración
            int coreCount = System.Environment.ProcessorCount / 2;
            int launchedThreads = 0;
            try
            {
                Airport[] airports = new Airport[_config.getFileCount()]; //Creamos los aeropuertos necesarios
                Task[] threads = new Task[_config.getFileCount()]; //Creamos los hilos necesarios
                for (int i = 0; i < _config.getFileCount(); i++)
                {
                    airports[i] = new Airport(@_config.getFile(i)); //Cargamos los ficheros en los aeropuertos
                }
                while (launchedThreads < _config.getFileCount())
                {
                    int pending = 16;
                    if ((launchedThreads + coreCount) > _config.getFileCount())
                    {
                        pending = launchedThreads - _config.getFileCount();
                    }
                    else
                        pending = coreCount;
                    System.Threading.CountdownEvent countdown = new System.Threading.CountdownEvent(pending); //Contador de sincronización
                    switch (_config.getAlgorithm()) //Iniciamos los hilos con el algortimo correspondiente
                    {
                        case ("greedy"):
                            for (int i = 0; i < pending; i++)
                            {
                                int copy = i;
                                threads[i] = Task.Run(() => Greedy.AlgGreedy(airports[copy], _config, copy, countdown));
                            }
                            break;
                        case ("buscalocal"):
                            for (int i = 0; i < pending; i++)
                            {
                                int copy = i + launchedThreads; //Es necesario para no recibir i y que los hilos intenten acceder a valores de otros hilos
                                threads[i] = Task.Run(() => LocalS.BLocal(airports[copy], _config, copy, countdown));
                            }
                            break;
                        case ("tabu"):
                            for (int i = 0; i < pending; i++)
                            {
                                int copy = i + launchedThreads;
                                threads[i] = Task.Run(() => TabuS.TabuSearch(airports[copy], _config, copy, countdown));
                            }
                            break;
                        case ("genetic"):
                            for (int i = 0; i < pending; i++)
                            {
                                int copy = i + launchedThreads;
                                threads[i] = Task.Run(() => Genetic.GeneticAlg(airports[copy], _config, copy, countdown));
                            }
                            break;

                        default:
                            Console.WriteLine("No se ha detectado algoritmo correctamente");
                            Console.WriteLine("Pulse cualquier tecla para continuar...");
                            Console.ReadKey();
                            break;
                    }
                    launchedThreads += pending;
                    countdown.Wait(); //Esperamos a terminar los hilos
                }
            }
            catch (FileNotFoundException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: el fichero no existe\n" + e.Message);
                Console.ResetColor();
                Console.ReadKey();
            }
            catch (IOException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: el fichero está dañado o no se puede leer\n" + e.Message);
                Console.ResetColor();
                Console.ReadKey();
            }
        }
    }
}