﻿using System;
using System.Diagnostics;

namespace Pr1MH
{
    static class Greedy
    {


        /*
         @brief Dada una matriz, devuelve el sumatorio por filas
         @param int [,] matriz matriz de que se haorce la sumatia por filas
t           @param int size tamaño de la matriz
         @return vector con las sumatorias
      */
        private static int[] sumatoria(int[,] matriz, int size)
        {
            int[] flujo = new int[size];
            int acum = 0;
            for (int i = 0; i < size; ++i)
            {
                acum = 0;
                for (int j = 0; j < size; ++j)
                {
                    acum += matriz[i, j];
                }
                flujo[i] = acum;
            }

            return flujo;
        }


        /*
            @brief Dado un array, busca el menor elemento y su posición
            @param int [] array array donde se busca
            @param in
        */
        private static int menor(int[] array, out int pos)
        {
            int menor = array[0];
            pos = 0;
            for (int i = 1; i < array.Length; ++i)
            {
                if (menor > array[i])
                {
                    menor = array[i];
                    pos = i;
                }

            }

            return menor;
        }

        private static int mayor(int[] array, out int pos)
        {
            int mayor = array[0];
            pos = 0;
            for (int i = 1; i < array.Length; ++i)
            {
                if (mayor < array[i])
                {
                    mayor = array[i];
                    pos = i;
                }
            }
            return mayor;
        }

        public static void AlgGreedy(Airport _airport, Config _config, int fileIndex, System.Threading.CountdownEvent countdown)
        {
            string ruta = Utility.file_path(_config, fileIndex);
            Logger file = new Logger(@ruta);//crea el archivo de escritura

            Stopwatch sw = new Stopwatch();
            sw.Start();

            int[] solucion = new int[_airport.getSize()]; //Array de enteros que es la solución
            bool[] paired = new bool[_airport.getSize()]; //Array que indica si esa posicion está resuelta

            for (int i = 0; i < paired.Length; i++)       //Inicializamos todos los valores como no resueltos
            {
                paired[i] = false;
            }

            int[] sum_flow = sumatoria(_airport.getFlow(), _airport.getSize()); //Sumatoria de flujo
            int[] sum_position = sumatoria(_airport.getPosition(), _airport.getSize()); //Sumatorio de posicion
            int max, min, counter = 0; //Contadores necesarios

            while (counter < _airport.getSize())
            {
                int posCercana = -1;
                int posPuerta = -1;
                min = menor(sum_position, out posCercana);//Menor distancia disponible
                max = mayor(sum_flow, out posPuerta);//Mayor flujo sin asignar
                solucion[posPuerta] = posCercana;
                paired[posPuerta] = true;
                sum_flow[posPuerta] = Int32.MinValue;
                sum_position[posCercana] = Int32.MaxValue;
                counter++;
            }
            int costo = Utility.evaluacion(_airport, solucion);
            sw.Stop();

            string bestStr = string.Join(" ", solucion);
            file.WriteLine("Fichero: " + _config.getFile(fileIndex));
            file.WriteLine("La solución obtenida es: " + bestStr);
            file.WriteLine("Costo: " + costo);
            file.WriteLine("Ha tardado: " + sw.ElapsedMilliseconds + "ms");
            file.Flush();

            countdown.Signal();
        }
    }
}
