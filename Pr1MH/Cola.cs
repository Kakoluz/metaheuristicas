﻿using System;
using System.Collections.Generic;

namespace Pr1MH
{
    //Esta clase es usada principalmente por la búsqueda tabú
    //Consiste en una cola de tamaño fijo que reemplaza los primeros elementos cuando se alcanza el límite
    public class Cola<T>
    {
        private Queue<T> cola;
        private int size;
        private T data;

        public Cola (int n)
        {
            if (n <= 0)
                throw new ArgumentException("Cola.cs: El tamaño de la cola debe ser mayor que 0");
            size = n;
            cola = new Queue<T>(n);
        }

        public T push (T data)
        {
            T returnValue = default(T);
            cola.Enqueue(data);
            if (cola.Count > size)
                returnValue = cola.Dequeue();
            return returnValue;
        }

        public T peek()
        {
            return cola.Peek();
        }

        public bool contains (T data)
        {
            return cola.Contains(data);
        }

        public void clear()
        {
            cola.Clear();
        }
    }
}
