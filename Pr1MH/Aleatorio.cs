using System;

namespace Pr1MH
{
     public class Aleatorio
     {
        private long seed;
        private long MASK = 2147483647;
        private int PRIME = 65539;
        private double SCALE = 0.4656612875e-9f;

        public Aleatorio (long seed)
        {
            this.seed = seed;
        }
        public Aleatorio ()
        {
            seed = DateTime.Now.Ticks;
        }
        public long get_seed()
        {
            return seed;
        }
        public float Rand()
        {
            return ((seed = ((seed * PRIME) & MASK)) * (float)SCALE);
        }
        public int Randint(int min, int max)
        {
            return (int)(min + (max - (min) + 1) * Rand());
        }
        public float RandFloat(float min, float max)
        {
            return (min + (max - (min)) * Rand());
        }
     }
}