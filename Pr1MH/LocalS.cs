﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Pr1MH
{
    static class LocalS
    {
        public static void BLocal(Airport _airport, Config _config, int fileIndex, System.Threading.CountdownEvent countdown) 
        {
            //Genera la ruta del fichero según el Sistema Operativo
            string ruta = Utility.file_path(_config,fileIndex);

            Logger file = new Logger(@ruta);//crea el archivo de escritura
            file.WriteLine("Se va a ejecutar el fichero: " + _config.getFile(fileIndex)); //se escriben los datos del archivo de datos
            file.WriteLine("semilla: " + _config.getSeed());
            file.WriteLine(DateTime.Now.ToString());
            file.WriteLine("");
            file.WriteLine("");

            //Inicia el cronometro
            Stopwatch sw = new Stopwatch();
            sw.Start();

            //se genera la primera la solución de forma aleatoria 
            int[] mejor = new int[_airport.getSize()];

            for (int i = 0; i < mejor.Length; ++i)
                mejor[i] = i;

            Random n = new Random((int) _config.getSeed());
            int x = (n.Next() % (_airport.getSize()));
           
            for (int i = 0; i < x; ++i) 
            {
                Utility.swap(ref mejor[n.Next(mejor.Length)],ref mejor[n.Next(mejor.Length)]);
            }
            int costomejor = Utility.evaluacion(_airport, mejor);

            //Se muestra la primera solución generada al azar
            string str = string.Join(" ", mejor);
            file.WriteLine("Solución inicial: " + str);
            file.WriteLine("Costo: " + costomejor);
            file.WriteLine("It: 0");

            //Generar la estructura para los vencinos
            int nvecinos = (_airport.getSize() * (_airport.getSize() - 1)) / 2;
            Tuple<int, int>[] vecino = new Tuple<int,int>[nvecinos];
            int tamv = 0;
            for (int i = 0; i < _airport.getSize(); i++)
            {
                for (int j = i + 1; j < _airport.getSize(); j++)
                {
                    vecino[tamv] = new Tuple<int, int>(i,j);
                    ++tamv;
                }
            }

            int intentos = 0; //Nuemro de vecinos que mira
            bool enc = false; //Verdadero si encuentra un vecino mejor
            int h = 0; //Declarado aqui por eficiencia 
            int it;
            for (it = 0; it < _config.getIterations() && intentos < 100; ++it)
            {
                do
                {
                    enc = false;
                    tamv = nvecinos - 1;

                    int[] mejorvecino = new int[_airport.getSize()];
                    int costemejorvecino = costomejor;
                    for (int j = 0; j < 10; j++)
                    {
                        h = n.Next(tamv);
                        int  r = vecino[h].Item1; //Posiciones que intercambia para generar al vecino
                        int  s = vecino[h].Item2;


                        int[] solpos = new int[mejor.Length];
                        Array.Copy(mejor, solpos, mejor.Length);

                        Utility.swap(ref solpos[r], ref solpos[s]); //Intercambia las posiciones
                        Utility.swap(ref vecino[h], ref vecino[tamv]); //Lleva el intercambio usado al final y reduce el tamaño para que no salga de nuevo
                        --tamv;

                        if (Utility.evaluacion(_airport, solpos) < costemejorvecino)
                        {
                            costemejorvecino = Utility.evaluacion(_airport, solpos);
                            Array.Copy(solpos, mejorvecino, mejorvecino.Length);

                        }
                        ++intentos;
                    }

                    if (costomejor > costemejorvecino)
                    {
                        enc = true;
                        costomejor = costemejorvecino;
                        Array.Copy(mejorvecino,mejor,mejor.Length);
                        file.WriteLine("---------------------------------------------------------------");
                        file.WriteLine("Movimiento: " + vecino[h].Item1 + "," + vecino[h].Item2);
                        file.WriteLine("Costo: " + costomejor);
                        file.WriteLine("It: " + it);
                    }

                } while (enc = false && intentos < 100);
            }
            //Detiene el cronometro, escribe la solución final y cierra el fichero
            sw.Stop();
            string bestStr = string.Join(" ", mejor);
            file.WriteLine("---------------------------------------------------------------");
            file.WriteLine( "La solución obtenida es: " + bestStr);
            file.WriteLine( "Costo: " + costomejor);
            file.WriteLine( "It: " + it);
            file.WriteLine( "Ha tardado: " + sw.ElapsedMilliseconds + "ms");
            file.Flush();
            countdown.Signal();
        }
    }
}