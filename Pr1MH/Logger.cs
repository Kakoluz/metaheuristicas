﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pr1MH
{
    class Logger
    {
        private List<string> lines;
        private string path;
        private System.IO.StreamWriter file;
        private int index;

        public Logger (string path)
        {
            lines = new List<string>();
            this.path = path;
            index = 0;
        }

        public void WriteLine(string line)
        {
            index++;
            lines.Add(line);
        }

        public void Write (string words)
        {
            lines[index] += words;
        }

        public void WriteLine(string line, int ind)
        {
            if (ind < lines.Count)
                this.lines[ind] = line;
        }

        public void Flush ()
        {
            file = new System.IO.StreamWriter(System.IO.File.Create(@path));
            for (int i = 0; i < lines.Count; i++)
            {
                lock (file)
                    file.WriteLine(lines[i]);
            }
            file.Flush();
            file.Close();
        }
    }
}
